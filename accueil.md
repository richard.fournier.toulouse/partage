# Description

Le contenu de ce site est également accessible sur https://gitlab.com/richard.fournier.toulouse/partage

Il s'agit de partager une pratique informatique.

Je ne suis pas informaticien, mais suite à la visite d'un magasin de hifi où tout était cher et le discours idiot, j'ai décidé de rentrer en résitance, non pas seulement contre les magasins de hifi, mais contre la tristesse de ce qu'est devenu notre rapport à l'informatique. Cette tristesse n'existait pas dans les années 1980 et je ne vois pas d'évolutions matérielles ou conceptuelles qui auraient pu la créer. Il me semble que c'est plutôt le contraire : il est difficile de croire que l'émergence des logiciels libres ou la baisse des prix, par exemple, aient comme effet de nous rendre tristes. Donc cette tristesse vient d'un ailleur qui n'est pas essentiel et que l'on peut combattre.

Dans cette bataille il suffit de mettre en avant ce qui nous plaît et chercher activement une alternative à chaque pratique qui a tendance à nous attrister. Et souvent ce n'est pas très dufficile. Personnellement, l'informatique me plaît quand elle

* favorise le partage (et le rire),
* n'est pas chère,
* ne pose de limites à aucun acteur (liberté, bataille sociale),
* se pense avant tout en termes esthétiques.

Dans ce magasin, le son n'était plus qu'une branche de l'informatique devenue affreuse, chère, le vendeur immergé dans cette laideur était devenu bête et triste, et si j'avais acheté un de ces appareils, à coup sûr il aurait bridé mon rapport à la musique, aurait fermé des portes. L'informatique était là comme elle est partout. Mais ce n'est pas partout qu'elle est moche. Il y a au contraire beaucoup de belle informatique, dans le monde de la muisque notamment. A nous de trouver les moyens de faire disparaître l'informatique moche, partout. 

# Unix

Pour accéder à un rapport fin à l'informatique, aujourd'hui il faut commencer par unix.

Il y un unix en natif sous windows, un unix à la base de mac et un unix à la base d'android. Malheureusement ce sont trois mauvais exemples et il va falloir accepter l'idée que ces industriels mettent à votre disposition des unix faciles d'accès mais inutilisables. Si vous êtes confrontés à l'un de ces trois systèmes, vous devrez investir un peu de temps pour installer, au coeur de ce système, un unix qui vous permette de travailler dans de bonnes conditions. On donnera ici un **exemple d'installation d'unix sous android**.

Bien sûr, vous n'aurez aucune de ces difficultés si vous utilsez un unix comme système principal. Aujourd'hui c'est possible facilement sur tout type de matériel. Vous pouvez donc prendre un vieux mac, effacer le système mac et mettre un unix à la place. Vous pouvez prendre un vieux PC sous windows, effacer windows et mettre un unix à la place. Vous pouvez bien sûr aussi acheter un ordinateur dédié et installer un unix à partir de zéro. On donnera ici un **exemple d'installation d'unix sur un raspberry pie**.

## Installer unix sur un téléphone android

### Installer le système

Vous pouvez installer UserLAnd et sélectionner debian lorsqu'on vous demandera de choisir une distribution de GNU/Linux. Parmi les unix existants, GNU/Linux est de loin le plus répandu. Parmi les distributions de GNU/Linux, debian est l'une des plus connues et des mieux documentées.

Ensuite vous rentrez votre mot de passe et vous lancez les deux commandes suivantes :

`sudo apt-get update`

`sudo apt-get upgrade`

Ces deux commandes mettent à jours tous les logiciels installés sur votre système. Vous avez donc intérêt à les exécuter régulièrement.

A ce stade, vous avez déjà un unix opérationnel et vous pouvez commencer à vous entrainer. Ce que vous saisissez dans la ligne de commande est interprété en language `bash`. Vous pouvez donc suivre n'importe quel tutoriel de `bash`.

### Installer quelques logiciels supplémentaires

Pour installer des nouveaux logiciels, il suffit de lancer une des commandes suivantes :

`sudo apt-get install paquet`

ou

`sudo apt-get install paquet1 paquet2 paquet3`

où `paquet`, `paquet1`, `paquet2` et `paquet3` sont les noms des paquets qui contiennent les logiciels que vous souhaitez installer.

Vous pouvez commencer par installer les premiers paquets suivants :

`sudo apt-get install vim openssh-client git rsync`

## Installer unix sur un raspberry pie

## Découvrir unix

On suppose ici que `vim`, `git` et `ssh` sont installés.

### bash

C'est le language que l'on utilise pour chaque commande. Voici une série de commandes `bash`que vous pouvez executer pour commencer :

`cd`

demande que le répertoire de travail soit votre répertoire personnel.

`pwd`

demande à voir le nom du répertoire de travail. Dans mon cas `/home/fournier`.

`mkdir bin`

crée un répertoire nommé bin dans le répertoire de travail. Dans mon cas `/home/fournier/bin`.

`git clone git@gitlab.com:richard.fournier.toulouse/partage.git`
 
crée un répertoire nommé `partage` qui est une copie du répertoire `partage` hébergé sur gitlab.

### vim

Pour écrire des textes ou écrire des programmes, éventuellement pour gérer la ligne de commande lorsque vous commencerez à saisir des commandes un peu longues, vous aurez besoin d'un éditeur de texte en ligne de commande. Le plus utilisé est `vim`.

### ssh

Pour interagir avec des ordianteurs distants (des serveurs), vous utiliserez `ssh`.

### git


# Un bon son pas cher : Conversion numérique-analogique

# Ancien texte sur Termux

Vous pouvez installer `Termux`. Il vous faudra aussi un éditeur, et sous `Termux` l'édition sera difficile. Donc vous pouvez installer `QuickEdit` et nous ferons l'edition sous android et non sous `Termux`. De façon générale, tout ce qui nécessite une minimum d'aisance avec le clavier ou de graphique sera fait sous android.

apt update
apt upgrade
termux-setup-storage

apt install nameOfPackage : installe le package
apt remove nameOfPackage : supprime un package
apt list : liste les packages disponibles
apt show nameOfPackage : fournit des informations sur un package
apt list –installed: fournit la liste des packages installés

htop
vim
python
git
wget
openssh
texlive-full

Volume Down+A : Move cursor to the beginning of line
Volume Down+C : Abort (send SIGINT to) current process
Volume Down+D : Logout of a terminal session
Volume Down+E : Move cursor to the end of line
Volume Down+K : Delete from cursor to the end of line
Volume Down+L : Clear the terminal
Volume Down+Z : Suspend (send SIGTSTP to) current process
Volume Up+L : | (the pipe character)
Volume Up+E : Escape key
Volume Up+T : Tab key
Volume Up+1 : F1 (and Volume Up+2 : F2, etc)
Volume Up+B : Alt+B, back a word when using readline
Volume Up+F : Alt+F, forward a word when using readline
Volume Up+W : Up arrow key
Volume Up+A : Left arrow key
Volume Up+S : Down arrow key
Volume Up+D : Right arrow key
Volume Up+. : Ctrl+(SIGQUIT)
Volume Up+U : _ (underscore)
Volume Up+X : Delete
Volume Up+P : Page Up
Volume Up+N : Page Down

copier les fichiers de .ssh dans document
creer .ssh
se mettre sous .ssh
utiliser cp à partir de là pour aller chercher les fichiers de .ssh dans document
effacer les fichiers de document





# Un bon son pas cher : Conversion numérique-analogique


